﻿using System.Net;
using System.Net.Mail;
using System.Security.Cryptography.X509Certificates;
using static Models.Constants.SystemConstants;

namespace BusinessLayer.EmailSMTP
{
    public class EmailServiceManager
    {

        #region Private Fields
        private static readonly object locker = new object();
        // Voltatile: value of the variable should never be cached because it might be changed by the operating system,
        private static volatile EmailServiceManager manager;
        #endregion

        #region Static Members 
        public static EmailServiceManager Instance
        {
            get
            {
                if (manager != null) return manager;
                lock (locker)
                {
                    if (manager == null)
                    {
                        manager = new EmailServiceManager();
                    }
                }
                return manager;
            }
            private set
            {
                lock (locker)
                {
                    manager = value;
                }
            }
        }

        public static void SendEmail(dynamic Model)
        {

            #region SMTP CONFIG

            //Define mail
            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient(SmtpConfiguration.HostName);
            mail.From = new MailAddress(SmtpConfiguration.FromEmailAddress);
            mail.To.Add(SmtpConfiguration.ToEmailAddress);

            mail.Subject = Model.Subject;
            mail.Body = Model.MessageBody;

            SmtpServer.Port = SmtpConfiguration.PortNumber;

            SmtpServer.Credentials = new System.Net.NetworkCredential(SmtpConfiguration.Username, SmtpConfiguration.Password);
            SmtpServer.EnableSsl = true;

            ServicePointManager.ServerCertificateValidationCallback = delegate (object sender, X509Certificate certificate, X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
            {
                return true;
            };

            SmtpServer.Send(mail);

            #endregion
        }
        #endregion


    }
}
