﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace BusinessLayer.Logging
{
    public class ExcelLogger
    {
        #region Fields
        private static ExcelLogger _logger;
        static object obj = new object();
        #endregion

        #region Properties
        public static ExcelLogger CurrentLogger
        {
            get
            {
                if (_logger == null)
                    _logger = new ExcelLogger();

                return _logger;
            }
        }
        #endregion

        #region Private Methods
        private void CreateExceptionFile(string identifier, string messageException, DateTime exceptionDate)
        {
            //Main MVC/API Config File
            string folderPath = ConfigurationManager.AppSettings["ErrorLogs"];
            string fileName;
            //Latest file is put in an active file
            string activeFilePath = folderPath + "ActiveFile.txt";
            string header = "Identifier,Error Message,Error Date" + Environment.NewLine;
            string content = identifier + "|" + messageException + "|" + exceptionDate;
            content = content.Replace(',', '-');
            content = content.Replace("\n", " ");
            content = content.Replace("\r", " ");
            content = content.Replace('|', ',') + Environment.NewLine;
            lock (obj)
            {
                try
                {
                    if (!File.Exists(activeFilePath))
                    {
                        string activeFileName = folderPath + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + " - " + DateTime.Now.Hour + DateTime.Now.Minute + DateTime.Now.Second + ".csv";
                        File.WriteAllText(activeFilePath, activeFileName);

                        File.WriteAllText(activeFileName, header);
                        File.AppendAllText(activeFileName, content);
                    }
                    else
                    {
                        using (StreamReader sr = new StreamReader(activeFilePath))
                        {
                            fileName = sr.ReadToEnd();
                        }
                        if (!string.IsNullOrEmpty(fileName) && File.Exists(fileName))
                        {
                            var fi = new FileInfo(fileName);
                            long size = fi.Length / 1048576;
                            if (size >= int.Parse(ConfigurationManager.AppSettings["MaximumSizeErrorLogMB"]))
                            {
                                fileName = folderPath + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + " - " + DateTime.Now.Hour + DateTime.Now.Minute + DateTime.Now.Second + ".csv";
                                File.WriteAllText(fileName, header);
                                File.AppendAllText(fileName, content);
                                File.WriteAllText(activeFilePath, fileName);
                            }
                            else
                            {
                                File.AppendAllText(fileName, content);
                            }

                        }
                        else
                        {
                            fileName = folderPath + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + " - " + DateTime.Now.Hour + DateTime.Now.Minute + DateTime.Now.Second + ".csv";
                            File.WriteAllText(fileName, header);
                            File.AppendAllText(fileName, content);
                            File.WriteAllText(activeFilePath, fileName);
                        }
                    }
                }
                catch (Exception exception)
                {
                    fileName = folderPath + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + " - " + DateTime.Now.Hour + DateTime.Now.Minute + DateTime.Now.Second + ".csv";
                    File.WriteAllText(fileName, header);
                    File.AppendAllText(fileName, content);
                    File.WriteAllText(activeFilePath, fileName);
                }
            }
        }
        #endregion

        #region Public Methods
        public void Log(string identifier, string messageException, DateTime exceptionDate)
        {
            try
            {
                CreateExceptionFile(identifier, messageException, exceptionDate);
            }
            catch (Exception)
            {
                //do nothing
            }
        }
        public void Log(Exception ex)
        {
            this.Log(ex.Source, ex.ToString(), DateTime.Now);
        }
        #endregion
    }
}

