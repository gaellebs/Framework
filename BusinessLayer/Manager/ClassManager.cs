﻿namespace BusinessLayer.Manager
{
    public class ClassManager
    {
        #region Private Fields
        private string _lovName;
        protected string LovName { get => _lovName; set => _lovName = value; }
        private string LovError { get; set; }
        #endregion

        #region Ctors
        public ClassManager(string lovname)
        {
            LovName = lovname;
        }
        public ClassManager(string lovname, string loverror) : this(lovname)
        {
            LovError = loverror;
        }
        #endregion
    }
}
