﻿using DataAccess.DataManager;

namespace BusinessLayer.Manager
{
    public class DefaultManager
    {
        #region Fields

        private string _connectionString;
        private DefaultDataManager _defaultDataManager;

        #endregion

        #region Ctor

        public DefaultManager(string connectionstring)
        {
            this._connectionString = connectionstring;
            _defaultDataManager = new DefaultDataManager(_connectionString);
        }

        #endregion

        #region Public Methods

        #endregion
    }
}
