﻿using Newtonsoft.Json;
using System;

namespace BusinessLayer.Serializaer
{
    //JSON SERIALIZER FOR OBJECTS INTO STRING
    public class ObjectSerializer
    {
        //Nuget Package Newtonsoft.Json
        private readonly JsonSerializerSettings _settings;
        public ObjectSerializer()
        {
            _settings = new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Objects,
                NullValueHandling = NullValueHandling.Ignore
            };
            _settings.Converters.Add(new Newtonsoft.Json.Converters.IsoDateTimeConverter());
        }
        public String Serialize(Object obj)
        {
            return JsonConvert.SerializeObject(obj, Formatting.None, _settings);
        }
        public String SerializeWithTypeNameHandelingNone(Object obj)
        {

            return JsonConvert.SerializeObject(obj, Formatting.None, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.None,
                NullValueHandling = NullValueHandling.Ignore
            }
            );
        }
        public T Deserialize<T>(String stringObject)
        {
            return JsonConvert.DeserializeObject<T>(stringObject, _settings);
        }
        public object Deserialize(String stringObject, Type type)
        {
            return JsonConvert.DeserializeObject(stringObject, type, _settings);
        }
        public object Deserialize(String stringObject)
        {
            return JsonConvert.DeserializeObject(stringObject, _settings);
        }
    }
}
