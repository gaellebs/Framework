﻿using Microsoft.SqlServer.Server;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace CLRProcedure
{
    public class CLRProcedures
    {
        #region JOB THAT TRIGGER API CALL

        [SqlProcedure]
        public static void MyDefaultProcedure(string action)
        {
            CallAPI(action);
        }

        #endregion

        #region Private Methods

        public static string InvokeRequest(string url, string data = null)
        {

            ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => { return true; };

            string responseBody = string.Empty;
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
            request.ContentType = "application/json;charset=utf-8";
            request.Method = "POST";

            if (data == null)
            {
                data = String.Empty;
            }

            // Write data 
            byte[] byteData = UTF8Encoding.UTF8.GetBytes(data);
            request.ContentLength = byteData.Length;
            using (Stream postStream = request.GetRequestStream())
            {
                postStream.Write(byteData, 0, byteData.Length);
            }
            // Get response 
            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                // Get the response stream 
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    responseBody = reader.ReadToEnd();
                    return responseBody;
                }
            }
        }

        public static List<T> ExecuteReader<T>(string cmdText, Func<IDataReader, T> mapper, IDictionary<string, object> parameters = null)
        {
            using (SqlConnection conn = new SqlConnection("context connection=true"))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(cmdText, conn))
                {
                    cmd.CommandType = System.Data.CommandType.Text;
                    if (parameters != null)
                    {
                        foreach (var param in parameters)
                        {
                            cmd.Parameters.AddWithValue(param.Key, param.Value);
                        }
                    }

                    using (var reader = cmd.ExecuteReader())
                    {
                        List<T> list = new List<T>();
                        while (reader.Read())
                        {
                            T instance = mapper.Invoke(reader);
                            list.Add(instance);
                        }
                        return list;
                    }
                }
            }
        }

        private static void CallAPI(String action, String ControllerName = null)
        {
            String host = ExecuteReader<String>("SELECT TOP 1 [SITENAME] FROM [SiteSettings] ", (reader) =>
            {
                return reader[0].ToString();
            }).First();

            String schedulerUrl = schedulerUrl = String.Format("https://{0}/api/{1}/{2}", host, ControllerName ?? "MyDefaultController", action);

            InvokeRequest(schedulerUrl);
        }

        #endregion

    }
}
