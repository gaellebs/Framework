﻿sp_configure 'clr enabled', 1;
	GO
		RECONFIGURE;
	GO
		ALTER DATABASE [MyDefaultDB]
			SET TRUSTWORTHY ON;
	GO


--Give My CLR BIN PATH
ALTER ASSEMBLY  CLRProcedure
FROM 'C:\Code\My Framework\GbsFramework\CLRProcedure\bin\Debug\CLRProcedure.dll'
WITH PERMISSION_SET = UNSAFE
GO


CREATE PROCEDURE MyDefaultProcedure
AS
EXTERNAL NAME  CLRProcedure.CLRProcedures.MyDefaultProcedure
GO

EXEC GeneratePdfWithCompletedRequests