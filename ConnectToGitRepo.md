Command line instructions
-----------------------------

Git global setup
------------------
git config --global user.name "Gaelle Bou Sleiman"

git config --global user.email "gaellebs@outlook.com"

Create a new repository
--------------------
git clone git@gitlab.com:gaellebs/TestProj.git

cd TestProj

touch README.md

git add README.md

git commit -m "add README"

git push -u origin master


Existing folder
------------------
cd existing_folder

git init

git remote add origin git@gitlab.com:gaellebs/TestProj.git

git add .

git commit -m "Initial commit"

git push -u origin master


Existing Git repository
------------------------
cd existing_repo

git remote rename origin old-origin

git remote add origin git@gitlab.com:gaellebs/TestProj.git

git push -u origin --all

git push -u origin --tags

---------------------------------------------
Extra commands:

git reset --mixed origin/master
git flow init -f

