﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Contracts
{
    public interface IBaseDataManager<T, E>
    {
        IList<T> SelectAll();
        IList<T> SelectByCriteria(string criteria);
        T SelectById(E id);
        E Insert(T category);
        void InsertBulk(List<T> categories);
        void Update(T category);
        void Delete(T category);
    }
}
