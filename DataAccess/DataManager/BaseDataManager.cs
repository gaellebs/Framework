﻿using DataAccess.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.DataManager
{
    public abstract class BaseDataManager
    {
        #region Private Fields

        private IDbConnection _connection;
        private string _connectionString;
        private Object _lock = new Object();

        #endregion


        #region Public Fields

        public IDbConnection DB {

            get {
                lock (_lock) {
                    if (this._connection == null) {
                        this._connection = new SqlConnection(DataAccessHelper.GetConnectionString(_connectionString));
                    }
                }
                return _connection;
            }
        }

        #endregion

        #region Ctor

        public BaseDataManager(string ConnectionString)
        {
            this._connectionString = ConnectionString;
        }

        #endregion

    }
}
