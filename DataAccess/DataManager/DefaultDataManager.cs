﻿using DataAccess.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.Entity;

namespace DataAccess.DataManager
{
    public class DefaultDataManager : BaseDataManager , IBaseDataManager<DefaultModel, Guid>
    {
        #region Ctor
        public DefaultDataManager(string ConnectionString)
            : base(ConnectionString) { }

        public void Delete(DefaultModel category)
        {
            throw new NotImplementedException();
        }

        public Guid Insert(DefaultModel category)
        {
            throw new NotImplementedException();
        }

        public void InsertBulk(List<DefaultModel> categories)
        {
            throw new NotImplementedException();
        }

        public IList<DefaultModel> SelectAll()
        {
            throw new NotImplementedException();
        }

        public IList<DefaultModel> SelectByCriteria(string criteria)
        {
            throw new NotImplementedException();
        }

        public DefaultModel SelectById(Guid id)
        {
            throw new NotImplementedException();
        }

        public void Update(DefaultModel category)
        {
            throw new NotImplementedException();
        }

        #endregion

    }
}
