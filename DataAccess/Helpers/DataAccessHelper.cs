﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Helpers
{
    public class DataAccessHelper
    {

        #region Public Methods

        public static string GetConnectionString(string cs)
        {
            string result = cs;
            var connectionString = ConfigurationManager.ConnectionStrings[cs];
            if (connectionString != null)
            {
                result = connectionString.ConnectionString;
            }
            if (!String.IsNullOrEmpty(result))
            {
                return result;
            }
            else
            {
                throw new InvalidOperationException(String.Format("Invalid Connection String {0}", cs));
            }
        }

        public static string GetProcedureName(string tableName, string actionName)
        {
            return string.Format("usp_{0}_{1}", tableName, actionName);
        }

        #endregion

    }
}
