﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Helpers
{
    public static class SiteConstants
    {

        #region Public Constants

        public static class ConnectionSettings
        {
            public const string DefaultDB = "OnlineServicesDB";
        }

        public static class TableName
        {
            public const string DefaultDBTable = "DefaultDBTable";
        }

        public static class ActionName
        {
            public const string DefaultDBAction = "usp_Table_action";
        }

        #endregion

    }
}
