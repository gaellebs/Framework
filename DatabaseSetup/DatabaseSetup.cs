﻿using Insight.Database.Schema;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseSetup
{
    public class DatabaseSetup
    {
        public static void Update(String connectionString, String sqlPath, String schemaName)
        {
            // load your SQL into a SchemaObjectCOllection
            SchemaObjectCollection schema = new SchemaObjectCollection();
            schema.Load(sqlPath);

            // automatically create the database
            SchemaInstaller.CreateDatabase(connectionString);

            // automatically install it, or upgrade it
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SchemaInstaller installer = new SchemaInstaller(connection);
                new SchemaEventConsoleLogger().Attach(installer);
                installer.Install(schemaName, schema);
            }
        }
    }
}
