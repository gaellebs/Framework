﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseSetup
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["DefaultDB"].ConnectionString;

            //Use PowerDesigner to generate script for your tables + autoproc scripts
            string path = Path.GetFullPath(@"C:\Code\proj-online-services\proj-online-services\OnlineServices.DataAccess\Sql\OnlineServicesSchema.sql");

            DatabaseSetup.Update(connectionString, path, "OnlineServicesSchema");
            Console.WriteLine("Press ENTER to continue.");
            Console.ReadLine();
        }
    }
    /********AutoProc scripts to Include in Preview on each Table*********/
        //-- AUTOPROC Table MyTableName
        //GO

        ////-- AUTOPROC IdTable MyTableName
        //GO

        //--AUTOPROC Select MyTableName Name = usp_MyTableName_Select
        //GO

        //--AUTOPROC Insert MyTableName Name = usp_MyTableName_Insert
        //GO

        //--AUTOPROC Update MyTableName Name = usp_MyTableName_Update
        //GO

        //--AUTOPROC Upsert MyTableName Name = usp_MyTableName_Upsert
        //GO

        //--AUTOPROC Delete MyTableName Name = usp_MyTableName_Delete
        //GO

        //--AUTOPROC Find MyTableName Name = usp_MyTableName_Find
        //GO

        //--AUTOPROC SelectMany MyTableName Name = usp_MyTableName_SelectMany
        //GO

        //--AUTOPROC InsertMany MyTableName Name = usp_MyTableName_InsertMany
        //GO

        //--AUTOPROC UpdateMany MyTableName Name = usp_MyTableName_UpdateMany
        //GO

        //--AUTOPROC UpsertMany MyTableName Name = usp_MyTableName_UpsertMany
        //GO

        //--AUTOPROC DeleteMany MyTableName Name = usp_MyTableName_DeleteMany
        //GO
    /**********************/
}
