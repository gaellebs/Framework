﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ExtensionsCore.Catalogs
{
    public class ObjectFactory
    {
        #region Private Fields

        private static readonly object SyncLock = new object();
        private static Dictionary<Type, HashSet<Type>> s_catalog;
        private static HashSet<Assembly> s_loadedAssemblies;
        private static Dictionary<Type, object> s_instances;
        #endregion

        #region Events
        public static event Action<Exception> LoadingException;
        #endregion

        #region Ctor
        static ObjectFactory()
        {
            s_instances = new Dictionary<Type, object>();
            s_catalog = new Dictionary<Type, HashSet<Type>>();
            s_loadedAssemblies = new HashSet<Assembly>();
        }

        #endregion

        #region Public Properties

        public static Boolean SearchLoadedAssembliesOnly { get; set; }

        #endregion

        #region Public Methods

        public static T Create<T>(params object[] parameters) where T : class
        {
            return Create<T>(true, parameters);
        }

        public static T Create<T>(bool singleton, params object[] parameters) where T : class
        {
            lock (SyncLock)
            {
                Type instanceType = typeof(T);
                if (singleton && s_instances.ContainsKey(instanceType))
                {
                    return s_instances[instanceType] as T;
                }

                if (!s_catalog.ContainsKey(instanceType))
                {
                    s_catalog.Add(instanceType, new HashSet<Type>());
                    InternalCreate<T>();
                }
                else
                {
                    if (s_catalog.Values.Count == 0)
                    {
                        InternalCreate<T>();
                    }
                }

                return CreateInstance(instanceType, singleton, parameters) as T;
            }
        }

        public static Object Create(Type type, params object[] parameters)
        {
            return Create(type, true, parameters);
        }

        public static Object Create(Type type, bool singleton, params object[] parameters)
        {
            lock (SyncLock)
            {
                if (singleton && s_instances.ContainsKey(type))
                {
                    return s_instances[type];
                }

                if (!s_catalog.ContainsKey(type))
                {
                    s_catalog.Add(type, new HashSet<Type>());
                    s_catalog[type].Add(type);
                }
                else
                {
                    if (s_catalog.Values.Count == 0)
                    {
                        s_catalog[type].Add(type);
                    }
                }

                return CreateInstance(type, singleton, parameters);
            }
        }

        public static void LoadAssembly(String assemblyFullName)
        {
            if (!String.IsNullOrEmpty(assemblyFullName))
            {
                try
                {
                    Assembly assembly = Assembly.Load(assemblyFullName);
                    if (assembly != null)
                    {
                        s_loadedAssemblies.Add(assembly);
                    }

                }
                catch { }
            }
        }

        public static void LoadAssembly(Assembly assembly)
        {
            if (assembly != null)
            {
                s_loadedAssemblies.Add(assembly);
            }
        }

        public static void RegisterInstance<T>(T instance)
        {
            if (instance != null)
            {
                Type instanceType = typeof(T);
                lock (SyncLock)
                {
                    if (s_instances.ContainsKey(instanceType))
                    {
                        s_instances.Remove(instanceType);
                    }
                    s_instances.Add(instanceType, instance);
                }
            }
        }
        public static void Clear()
        {
            lock (SyncLock)
            {
                s_instances.Clear();
                s_loadedAssemblies.Clear();
                s_catalog.Clear();
            }
        }
        #endregion

        #region Private Methods

        private static Object CreateInstance(HashSet<Type> typeSet, params object[] parameters)
        {
            Object instance = null;
            Type typeToActivate = typeSet.FirstOrDefault(n => !n.IsAbstract);
            if (typeToActivate != null)
            {
                instance = Activator.CreateInstance(typeToActivate, parameters);
            }
            return instance;
        }
        private static Object CreateInstance(Type type, params object[] parameters)
        {
            Object instance = null;
            if (!type.IsAbstract)
            {
                instance = Activator.CreateInstance(type, parameters);
            }
            return instance;
        }
        private static object CreateInstance(Type type, bool singleton, object[] parameters)
        {
            Object instance = null;
            HashSet<Type> matches = s_catalog[type];
            instance = CreateInstance(matches, parameters);
            if (instance == null)
            {
                instance = CreateInstance(type, parameters);
            }
            if (singleton && instance != null)
            {
                s_instances.Add(type, instance);
            }

            return instance;
        }
        private static void InternalCreate<T>() where T : class
        {
            Type instanceType = typeof(T);
            IEnumerable<Assembly> assemblies = null;

            try
            {
                if (SearchLoadedAssembliesOnly)
                {
                    assemblies = new List<Assembly>(s_loadedAssemblies);
                }
                else
                {
                    assemblies = AppDomain.CurrentDomain.GetAssemblies()
                        .Where(n => !n.FullName.StartsWith("mscorlib")
                                    && !n.FullName.StartsWith("Microsoft")
                                    && !n.FullName.StartsWith("Oracle")
                                    && !n.FullName.StartsWith("Telerik")
                                    && !n.IsDynamic
                                    && !n.FullName.StartsWith("vshost")
                                    && !n.FullName.StartsWith("System"));
                }
            }
            catch (Exception ex)
            {
                //LogLoadingException(ex);
                FireLoadingException(ex);
                throw;
            }

            foreach (Assembly assembly in assemblies)
            {
                IEnumerable<Type> assemblyTypes = null;
                try
                {
                    assemblyTypes = assembly.GetTypes()
                        .Where(n => !n.AssemblyQualifiedName.StartsWith("System."));
                }
                catch (Exception ex)
                {
                    //LogLoadingException(ex);
                    FireLoadingException(ex);
                    throw;
                }

                if (instanceType.IsClass)
                {
                    foreach (Type assemblyType in assemblyTypes)
                    {
                        if (assemblyType.IsSubclassOf(instanceType))
                        {
                            s_catalog[instanceType].Add(assemblyType);
                            break;
                        }
                    }
                }
                if (instanceType.IsInterface)
                {
                    foreach (Type assemblyType in assemblyTypes)
                    {
                        if (assemblyType.GetInterface(instanceType.Name) != null)
                        {
                            s_catalog[instanceType].Add(assemblyType);
                            break;
                        }
                    }
                }
            }
        }

        //private static void LogLoadingException(Exception ex)
        //{
        //    try
        //    {
        //        string stringToLog = ex.ToStringWithLoaderExceptions();
        //        EventLog.WriteEntry("ObjectFactory", stringToLog, EventLogEntryType.Error);
        //    }
        //    catch
        //    {
        //    }
        //}

        private static void FireLoadingException(Exception ex)
        {
            if (LoadingException != null)
            {
                LoadingException(ex);
            }
        }

        #endregion
    }
}
