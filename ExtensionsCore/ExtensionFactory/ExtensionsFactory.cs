﻿using ExtensionsCore.Catalogs;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtensionsCore.ExtensionFactory
{
    public class ExtensionsFactory
    {
        #region Fields
        private static bool _initialized;
        private static readonly object lockObject = new object();
        #endregion

        #region Public methods

        public static void Initialize()
        {
            var assemblyName = ConfigurationManager.AppSettings["PreSetupHandlerExtensionsAssembly"];
            if (string.IsNullOrEmpty(assemblyName)) return;

            if (!_initialized)
            {
                // We avoid locking too often by doing the double-check on the bool variable.
                lock (lockObject)
                {
                    if (_initialized) return;
                    ObjectFactory.SearchLoadedAssembliesOnly = true;
                    ObjectFactory.LoadAssembly(assemblyName);
                    _initialized = true;
                }
            }
            else
            {
                // Just in case the Initialize() function is called from another site in the
                // same app pool.
                ObjectFactory.LoadAssembly(assemblyName);
            }
        }

        public static T Create<T>(bool singleton = false, params object[] parameters) where T : class
        {
            return ObjectFactory.Create<T>(singleton, parameters);
        }
        #endregion
    }
}
