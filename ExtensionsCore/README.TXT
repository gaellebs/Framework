﻿How to Use it 
--------------
- How to talk with diff apps through contracts
- extend a given contract within ur app not to overwrite the main app used
- In Global.asax.cs
	add in the Application_Start() - > ExtensionsFactory.Initialize();


ExtensionsFactory.ObjectFactory<InterfaceClass>
ex:  var setupHandler = ExtensionsFactory.Create<ISetupHandler>();

In My App
---------
  public class SetupHandler : ISetupHandler
    {        
        public void PreSetupHandler(IRequirementsMappingResult mappingResult)
        {
			//DO STUFF
        }
    }

In External App
---------------
 public interface ISetupHandler
 {
   void PreSetupHandler(IRequirementsMappingResult mappingResults);
 }