﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Constants
{
    public class SystemConstants
    {
        public static class ConnectionSettings
        {
            public const string DefaultDB = "DefaultDB";
        }
        public static class TableName
        {
            public const string DefaultDbTable = "DefaultDbTable";

        }
        public static class ProcedureNames
        {
            public const string Find = "Find";
            public const string Query = "Query";
            public const string SelectAll = "SelectAll";
            public const string Insert = "Insert";

        }

        public static class SmtpConfiguration
        {
            public const string HostName = "smtp.gmail.com";
            //put your own gmail after configured
            public const string FromEmailAddress = "digiminds.dev@gmail.com";
            public const string ToEmailAddress = "digiminds.dev@gmail.com";
            public const int PortNumber = 587;
            public const string Username = "digiminds.dev@gmail.com";
            public const string Password = "digip@ssw0rd";
        }
    }
}
